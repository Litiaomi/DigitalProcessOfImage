rightans=0
for i=1:10
	accuracy=zeros(10);
    trainSpecialValue=zeros(8,4,5);%�洢ѵ��������������ֵ
    testSpecialValue=zeros(8,4,5);%�洢���Լ�����������ֵ
    trainMartix=zeros(8,4);%�洢ѵ����ͼ������
    testMartix=zeros(8,4);%�洢���Լ�ͼ������
	testInitialType=zeros(8,4);%记录刚开始测试集的图片类型
	testPreType=zeros(8,4);%记录预测的测试集图片类型
    order=randperm(8);%��ͼ���������
    trainorder=order(1:4);%��ȡѵ����ͼ����
    testorder=order(5:8);%��ȡ���Ի�ͼ����
    for j=1:8
        for k=1:4
			testInitialType(j,k)=j;
        	trainMartix(j,k)=j*10+trainorder(k);%图片名称
        	testMartix(j,k)=j*10+testorder(j);%图片名称
       		trainglcm=graycomatrix(strcat(int2str(trainMartix(j,k)),'.jpg'));%获得图片
        	trainSpecialValue(j,k,1)=graycoprops(trainglcm,'Contrast');%对比度
        	trainSpecialValue(j,k,2)=graycoprops(trainglcm,'Correlation');%相关度
        	trainSpecialValue(j,k,3)=graycoprops(trainglcm,'Energy');%能量
        	trainSpecialValue(j,k,4)=graycoprops(trainglcm,'Homogemeity');%均匀度
			trainSpecialValue(j,k,5)=entropy(trainMartix(j,k));%图片的熵

        	testglcm=graycomatrix((strcat(int2str(testMartix(j,k)),'.jpg'));%获得图片
        	testSpecialValue(j,k,1)=graycoprops( testglcm,'Contrast');%对比度
        	testSpecialValue(j,k,2)=graycoprops( testglcm,'Correlation');%相关度
       		testSpecialValue(j,k,3)=graycoprops( testglcm,'Energy');%能量
        	testSpecialValue(j,k,4)=graycoprops( testglcm,'Homogemeity');%ͬ均匀度
			testSpecialValue(j,k,5)=entropy(testMartix(j,k));%图片的熵
        end
     end

	rightNum=0;
	for j=1:8
        for k=1:4
			temp = 100;
			for m=1:8
				for n=1:4
					t=(testSpecialValue(j,k,1)-trainSpecialValue(m,n,1))^2+ %计算欧式距离
					(testSpecialValue(j,k,2)-trainSpecialValue(m,n,2))^2+
					(testSpecialValue(j,k,3)-trainSpecialValue(m,n,3))^2+
					(testSpecialValue(j,k,4)-trainSpecialValue(m,n,4))^2+
					(testSpecialValue(j,k,5)-trainSpecialValue(m,n,5))^2;
					if(t<temp)
					{
						temp=t;
						testPreType(j,k)=m;
					}
			%统计正确率
			if(testPreType(j,k)==testInitialType(j,k))
			{
				rightNum+=1;
			}
		end
	end
	accuracy(i)=rightNum/32;%正确率的记录
end
accuracy;%正确率矩阵