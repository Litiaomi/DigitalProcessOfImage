﻿function y = entropy(I)
    I=imread(strcat(int2str(I,'.jpg'));
    x=double(I);
    n=256;
    xh=hist(x(:),n);
    xh=xh/sum(xh(:));
    i=find(xh);
    y=-sum(xh(i).*log2(xh(i)));
end